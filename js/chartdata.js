// This code is licensed under the MIT License. Please see LICENSE.txt in the base directory for more information.

// 2017-2018 chart data
// Summary array format: Formatted Date (0), Outcome (1), Prediction Outcome (2), Prediction Count (3)
// Add your own chart data by following the format shown here. The name for the variables should match what's in the section
// of the homepage to pull up the right variables in chartdraw.

var jan172018_summary = ["January 17, 2018", "Snow day", "Success", "8"]
var jan172018_avg = "<b>Average 1-hour delay chance:</b> 100.00% (0.00% std. deviation)<br><b>Average 2-hour delay chance:</b> 100.00% (0.00% std. deviation)<br><b>Average 3-hour delay chance</b>: 97.63% (5.15% std. deviation)<br><b>Average Snow Day chance:</b> 94.63% (7.67% std. deviation)"
var jan172018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("1/16/2018 18:47:00 GMT-0500"), 100, 100, 100, 100],
    [new Date("1/16/2018 19:10:00 GMT-0500"), 100, 100, 100, 100],
    [new Date("1/16/2018 20:03:00 GMT-0500"), 100, 100, 99, 98],
    [new Date("1/16/2018 20:40:00 GMT-0500"), 100, 100, 98, 97],
    [new Date("1/16/2018 21:40:00 GMT-0500"), 100, 100, 85, 77],
    [new Date("1/16/2018 22:05:00 GMT-0500"), 100, 100, 99, 91],
    [new Date("1/16/2018 22:37:00 GMT-0500"), 100, 100, 100, 96],
    [new Date("1/17/2018 05:26:00 GMT-0500"), 100, 100, 100, 98]
]

var jan182018_summary = ["January 18, 2018", "Nothing", "Success", "2"]
var jan182018_avg = "<b>Average 1-hour delay chance:</b> 17.33% (10.79% std. deviation)<br><b>Average 2-hour delay chance:</b> 13.67% (7.77% std. deviation)<br><b>Average 3-hour delay chance:</b> 2.67% (4.62% std. deviation)<br><b>Average Snow Day chance:</b> 1.33% (2.31% std. deviation)"
var jan182018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("1/16/2018 18:47:00 GMT-0500"), 22, 16, 8, 4],
    [new Date("1/17/2018 10:08:00 GMT-0500"), 25, 20, 0, 0],
    [new Date("1/17/2018 20:37:00 GMT-0500"), 5, 5, 0, 0],
]

var jan302018_summary = ["January 30, 2018", "2-hour delay", "Failure", "1"]
var jan302018_avg = "<b>Average 1-hour delay chance:</b> 20.00% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 15.00% (undef std. deviation)<br><b>Average 3-hour delay chance:</b> 0.00% (undef std. deviation)<br><b>Average Snow Day chance:</b> 0.00% (undef std. deviation)"
var jan302018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("1/29/2018 19:30:00 GMT-0500"), 20, 15, 0, 0]
]

var feb22018_summary = ["February 2, 2018", "2-hour delay", "Success", "1"]
var feb22018_avg = "<b>Average 1-hour delay chance:</b> 95.00% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 75.00% (undef std. deviation)<br><b>Average 3-hour delay chance:</b> 55.00% (undef std. deviation)<br><b>Average Snow Day chance:</b> 5.00% (undef std. deviation)"
var feb22018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("2/1/2018 14:35:00 GMT-0500"), 95, 75, 55, 5]
]

var feb52018_summary = ["February 5, 2018", "Nothing", "Failure", "1"]
var feb52018_avg = "<b>Average 1-hour delay chance:</b> 90.00% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 85.00% (undef std. deviation)<br><b>Average 3-hour delay chance:</b> 40.00% (undef std. deviation)<br><b>Average Snow Day chance:</b> 15.00% (undef std. deviation)"
var feb52018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("1/28/2018 12:35:00 GMT-0500"), 90, 85, 40, 15]
]

var feb72018_summary = ["February 7, 2018", "Snow day", "Success", "2"]
var feb72018_avg = "<b>Average 1-hour delay chance:</b> 0.00% (0.00% std. deviation)<br><b>Average 2-hour delay chance:</b> 0.00% (0.00% std. deviation)<br><b>Average 3-hour delay chance:</b> 0.00% (0.00% std. deviation)<br><b>Average Snow Day chance:</b> 99.45% (0.64% std. deviation)<br><b>Average early dismissal chance:</b> 25.00% (undef std. deviation)"
var feb72018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance", "Early dismissal chance"],
    [new Date("2/6/2018 10:05:00 GMT-0500"), 0, 0, 0, 99, null],
    [new Date("2/6/2018 17:44:00 GMT-0500"), 0, 0, 0, 99.90, 25]
]

var feb82018_summary = ["February 8, 2018", "Nothing", "Failure", "1"]
var feb82018_avg = "<b>Average 1-hour delay chance:</b> 55.00% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 45.00% (undef std. deviation)<br><b>Average 3-hour delay chance:</b> 35.00% (undef std. deviation)<br><b>Average Snow Day chance:</b> 0.00% (undef std. deviation)"
var feb82018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("2/7/2018 21:43:00 GMT-0500"), 55, 45, 35, 0]
]

var mar22018_summary = ["March 2, 2018", "Snow day", "Success", "5"]
var mar22018_avg = "<b>Average 1-hour delay chance:</b> 0.00% (0.00% std. deviation)<br><b>Average 2-hour delay chance:</b> 7.50% (15.00% std. deviation)<br><b>Average 3-hour delay chance:</b> 7.50% (8.66% std. deviation)<br><b>Average Snow Day chance:</b> 70.00% (43.59% std. deviation)<br><b>Average early dismissal chance:</b> 65.00% (undef std. deviation)"
var mar22018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance", "Early dismissal chance"],
    [new Date("3/1/2018 09:15:00 GMT-0500"), 0, 30, 15, 20, null],
    [new Date("3/1/2018 09:40:00 GMT-0500"), 0, 0, 15, null, 65],
    [new Date("3/1/2018 11:10:00 GMT-0500"), null, null, null, null, null],
    [new Date("3/1/2018 19:00:00 GMT-0500"), 0, 0, 0, 90, null],
    [new Date("3/1/2018 22:10:00 GMT-0500"), 0, 0, 0, 100, null]
]

var mar72018_summary = ["March 7, 2018", "Snow day", "Success", "2"]
var mar72018_avg = "<b>Average 1-hour delay chance:</b> 0.00% (0.00% std. deviation)<br><b>Average 2-hour delay chance:</b> 0.00% (0.00% std. deviation)<br><b>Average 3-hour delay chance:</b> 0.00% (0.00% std. deviation)<br><b>Average Snow Day chance:</b> 97.50% (3.54% std. deviation)"
var mar72018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("3/3/2018 19:53:00 GMT-0500"), 0, 0, 0, 95],
    [new Date("3/6/2018 07:25:00 GMT-0500"), 0, 0, 0, 100]
]

var mar82018_summary = ["March 8, 2018", "Snow day", "Success", "3"]
var mar82018_avg = "<b>Average Snow Day chance:</b> 69.67% (12.86% std. deviation)"
var mar82018_data = [
    ["Prediction Time", "Snow day chance"],
    [new Date("3/7/2018 21:40:00 GMT-0500"), 55],
    [new Date("3/7/2018 22:25:00 GMT-0500"), 75],
    [new Date("3/7/2018 22:50:00 GMT-0500"), 79]
]

var mar132018_summary = ["March 13, 2018", "Snow day", "Success", "6"]
var mar132018_avg = "<b>Average 1-hour delay chance:</b> 75.00% (50.00% std. deviation)<br><b>Average 2-hour delay chance:</b> 73.50% (49.02% std. deviation)<br><b>Average 3-hour delay chance:</b> 82.00% (21.43% std. deviation)<br><b>Average Snow Day chance:</b> 82.16% (13.35% std. deviation)"
var mar132018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("3/12/2018 08:55:00 GMT-0400"), 100, 97, 93, 90],
    [new Date("3/12/2018 08:58:00 GMT-0400"), 100, 97, 90, 85],
    [new Date("3/12/2018 14:18:00 GMT-0400"), 100, 100, 95, 85],
    [new Date("3/12/2018 18:50:00 GMT-0400"), 0, 0, 50, 100],
    [new Date("3/13/2018 06:10:00 GMT-0400"), null, null, null, 65],
    [new Date("3/13/2018 07:40:00 GMT-0400"), null, null, null, 68]
]

var mar212018_summary = ["March 21, 2018", "Snow day", "Success", "5"]
var mar212018_avg = "<b>Average 2-hour delay chance:</b> 3.00% (6.71% std. deviation)<br><b>Average 3-hour delay chance:</b> 4.00% (8.94% std. deviation)<br><b>Average Snow Day chance:</b> 59.80% (25.84% std. deviation)<br><b>Average early dismissal chance:</b> 38.75% (13.15% std. deviation)"
var mar212018_data = [
    ["Prediction Time", "2-hour delay chance", "3-hour delay chance", "Snow day chance", "Early dismissal chance"],
    [new Date("3/19/2018 15:00:00 GMT-0400"), 15, 20, 40, 45],
    [new Date("3/20/2018 07:30:00 GMT-0400"), 0, 0, 35, 50],
    [new Date("3/20/2018 10:30:00 GMT-0400"), 0, 0, 55, 40],
    [new Date("3/20/2018 14:30:00 GMT-0400"), 0, 0, 70, 20],
    [new Date("3/20/2018 21:30:00 GMT-0400"), 0, 0, 99, null]
]

var mar222018_summary = ["March 22, 2018", "1-hour delay", "Failure", "1"]
var mar222018_avg = "<b>Average 1-hour delay chance:</b> 5% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 0% (undef std. deviation)"
var mar222018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance"],
    [new Date("3/21/2018 21:05:00 GMT-0400"), 5, 0]
]

var may162018_summary = ["May 16, 2018", "Tornado (Snow) Day", "Success", "1"]
var may162018_avg = "<b>Average 1-hour delay chance:</b> 99.00% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 95.00% (undef std. deviation)<br><b>Average 3-hour delay chance:</b> 70.00% (undef std. deviation)<br><b>Average Snow Day chance:</b> 55.00% (undef std. deviation)"
var may162018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("5/15/2018 17:40:00 GMT-0400"), 99, 95, 70, 55]
]

var may172018_summary = ["May 17, 2018", "Nothing", "Failure", "1"]
var may172018_avg = "<b>Average 1-hour delay chance:</b> 100.00% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 90.00% (undef std. deviation)<br><b>Average 3-hour delay chance:</b> 75.00% (undef std. deviation)<br><b>Average Snow Day chance:</b> 60.00% (undef std. deviation)"
var may172018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("5/16/2018 09:45:00 GMT-0400"), 100, 90, 75, 60]
]

// 2018-2019 data

var nov162018_summary = ["November 15 & 16, 2018", "Early dismissal (15th) & Snow day (16th)", "Success (x2)", "19"]
var nov162018_avg = "<b>Average 1-hour delay chance:</b> 89.74% (22.71% std. deviation)<br><b>Average 2-hour delay chance:</b> 82.42% (26.39% std. deviation)<br><b>Average 3-hour delay chance:</b> 51.47% (36.36% std. deviation)<br><b>Average Snow Day chance:</b> 57.68% (33.86% std. deviation)<br><b>Average early dismissal chance:</b> 61.66% (28.43% std. deviation)<br><b>Average after-school activities cancelled chance:</b> 98.00% (2.65% std. deviation)"
var nov162018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance", "Early dismissal chance", "After school activities cancelled chance"],
    [new Date("11/11/2018 23:30:00 GMT-0500"), 24, 22, 12, 4, null, null],
    [new Date("11/12/2018 08:30:00 GMT-0500"), 31, 21, 8, 1, null, null],
    [new Date("11/13/2018 07:00:00 GMT-0500"), 80, 60, 35, 5, null, null],
    [new Date("11/13/2018 18:30:00 GMT-0500"), 95, 74, 42, 11, null, null],
    [new Date("11/14/2018 07:45:00 GMT-0500"), 90, 62, 47, 29, null, null],
    [new Date("11/14/2018 10:45:00 GMT-0500"), 85, 55, 53, 32, null, null],
    [new Date("11/14/2018 15:45:00 GMT-0500"), 100, 85, 70, 50, null, null],
    [new Date("11/14/2018 19:00:00 GMT-0500"), 100, 90, 78, 63, 30, 95],
    [new Date("11/15/2018 07:00:00 GMT-0500"), 100, 97, 77, 61, 85, 99],
    [new Date("11/15/2018 09:00:00 GMT-0500"), 100, 100, 83, 70, 70, 100],
    [new Date("11/15/2018 16:00:00 GMT-0500"), 100, 100, 86, 75, null, null],
    [new Date("11/15/2018 17:30:00 GMT-0500"), 100, 100, 90, 82, null, null],
    [new Date("11/15/2018 19:30:00 GMT-0500"), 100, 100, 93, 85, null, null],
    [new Date("11/15/2018 21:45:00 GMT-0500"), 100, 100, 94, 89, null, null],
    [new Date("11/15/2018 23:20:00 GMT-0500"), 100, 100, 95, 90, null, null],
    [new Date("11/16/2018 06:40:00 GMT-0500"), 100, 100, 10, 68, null, null],
    [new Date("11/16/2018 07:10:00 GMT-0500"), 100, 100, 4, 82, null, null],
    [new Date("11/16/2018 07:15:00 GMT-0500"), 100, 100, 1, 99, null, null],
    [new Date("11/16/2018 07:30:00 GMT-0500"), 100, 100, 0, 100, null, null]
]

var nov202018_summary = ["November 20, 2018", "Nothing", "Success", "1"]
var nov202018_avg = "<b>Average 1-hour delay chance:</b> 0.00% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 0.00% (undef std. deviation)<br><b>Average 3-hour delay chance:</b> 0.00% (undef std. deviation)<br><b>Average Snow Day chance:</b> 0.00% (undef std. deviation)"
var nov202018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("11/19/2018 10:15:00 GMT-0500"), 0, 0, 0, 0]
]

var dec132018_summary = ["December 13, 2018", "Nothing", "Success", "1"]
var dec132018_avg = "<b>Average early dismissal chance:</b> 1.00% (undef std. deviation)<br><b>Average after-school activities cancelled:</b> 9.00% (undef std. deviation)"
var dec132018_data = [
    ["Prediction Time", "Early dismissal chance", "After-school activities cancelled chance"],
    [new Date("12/13/2018 10:30:00 GMT-0500"), 1, 9]
]

var dec172018_summary = ["December 17, 2018", "Nothing", "Success", "4"]
var dec172018_avg = "<b>Average 1-hour delay chance:</b> 36.25% (25.82% std. deviation)<br><b>Average 2-hour delay chance:</b> 25.75% (18.77% std. deviation)<br><b>Average 3-hour delay chance:</b> 18.00% (14.35% std. deviation)<br><b>Average Snow Day chance:</b> 0.25% (0.50% std. deviation)"
var dec172018_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("12/16/2018 19:00:00 GMT-0500"), 40, 30, 20, 0],
    [new Date("12/16/2018 21:45:00 GMT-0500"), 61, 45, 35, 1],
    [new Date("12/16/2018 23:15:00 GMT-0500"), 44, 28, 17, 0],
    [new Date("12/17/2018 05:35:00 GMT-0500"), 0, 0, 0, 0]
]

var jan82019_summary = ["January 8, 2019", "2-hour delay", "Failure", "1"]
var jan82019_avg = "<b>Average 1-hour delay chance:</b> 5.00% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 3.00% (undef std. deviation)<br><b>Average 3-hour delay chance:</b> 1.00% (undef std. deviation)<br><b>Average Snow Day chance:</b> 0.00% (undef std. deviation)"
var jan82019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("1/7/2019 08:45:00 GMT-0500"), 5, 3, 1, 0]
]

var jan182019_summary = ["January 18, 2019", "Nothing", "Somewhat successful", "10"]
var jan182019_avg = "<b>Average 1-hour delay chance:</b> 39.70% (31.88% std. deviation)<br><b>Average 2-hour delay chance:</b> 36.90% (23.92% std. deviation)<br><b>Average 3-hour delay chance:</b> 26.10% (18.83% std. deviation)<br><b>Average Snow Day chance:</b> 12.10% (14.73% std. deviation)"
var jan182019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("1/13/2019 19:30:00 GMT-0500"), 0, 1, 8, 10],
    [new Date("1/15/2019 07:30:00 GMT-0500"), 40, 24, 20, 17],
    [new Date("1/15/2019 11:00:00 GMT-0500"), 0, 39, 41, 21],
    [new Date("1/16/2019 07:30:00 GMT-0500"), 35, 58, 55, 49],
    [new Date("1/16/2019 23:45:00 GMT-0500"), 70, 60, 45, 10],
    [new Date("1/17/2019 07:30:00 GMT-0500"), 73, 56, 32, 6],
    [new Date("1/17/2019 10:45:00 GMT-0500"), 77, 63, 38, 7],
    [new Date("1/17/2019 17:15:00 GMT-0500"), 70, 46, 18, 1],
    [new Date("1/17/2019 22:30:00 GMT-0500"), 32, 22, 4, 0],
    [new Date("1/18/2019 05:45:00 GMT-0500"), 0, 0, 0, 0]
]

var jan302019_summary = ["January 29 & 30, 2019", "Early dismissal (29th) & Snow day (30th)", "Success (x2)", "12"]
var jan302019_avg = "<b>Average 1-hour delay chance:</b> 97.67% (3.67% std. deviation)<br><b>Average 2-hour delay chance:</b> 91.08% (7.35% std. deviation)<br><b>Average 3-hour delay chance:</b> 50.08% (16.54% std. deviation)<br><b>Average Snow Day chance:</b> 23.08% (8.65% std. deviation)<br><b>Average Snow Day chance:</b> 66.00% (6.71% std. deviation)"
var jan302019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance", "Early dismissal chance"],
    [new Date("1/28/2019 07:30:00 GMT-0500"), 90, 79, 52, 22, 58],
    [new Date("1/28/2019 11:00:00 GMT-0500"), 93, 80, 50, 20, 61],
    [new Date("1/28/2019 21:00:00 GMT-0500"), 93, 87, 50, 23, 69],
    [new Date("1/29/2019 00:00:00 GMT-0500"), 96, 84, 55, 23, 67],
    [new Date("1/29/2019 07:00:00 GMT-0500"), 100, 89, 52, 23, 75],
    [new Date("1/29/2019 09:00:00 GMT-0500"), 100, 92, 53, 23, null],
    [new Date("1/29/2019 10:30:00 GMT-0500"), 100, 94, 62, 32, null],
    [new Date("1/29/2019 13:00:00 GMT-0500"), 100, 95, 63, 34, null],
    [new Date("1/29/2019 17:30:00 GMT-0500"), 100, 94, 62, 31, null],
    [new Date("1/29/2019 20:00:00 GMT-0500"), 100, 99, 53, 26, null],
    [new Date("1/29/2019 21:30:00 GMT-0500"), 100, 100, 49, 20, null],
    [new Date("1/30/2019 05:45:00 GMT-0500"), 100, 100, 0, 0, null]
]

var jan312019_summary = ["January 31, 2019", "2-hour delay", "Success", "3"]
var jan312019_avg = "<b>Average 1-hour delay chance:</b> 83.50% (21.92% std. deviation)<br><b>Average 2-hour delay chance:</b> 61.50% (9.19% std. deviation)<br><b>Average 3-hour delay chance:</b> 54.50% (2.12% std. deviation)<br><b>Average Snow Day chance:</b> 36.00% chance (26.06% std. deviation)"
var jan312019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("1/30/2019 09:30:00 GMT-0500"), 68, 55, 53, 49],
    [new Date("1/30/2019 12:00:00 GMT-0500"), 99, 68, 56, 53],
    [new Date("1/30/2019 21:00:00 GMT-0500"), null, null, null, 6]
]

var feb122019_summary = ["February 12, 2019", "Snow day", "Success", "5"]
var feb122019_avg = "<b>Average 1-hour delay chance:</b> 0.00% (undef std. deviation)<br><b>Average 2-hour delay chance:</b> 0.00% (undef std. deviation)<br><b>Average 3-hour delay chance:</b> 5.00% (undef std. deviation)<br><b>Average Snow Day chance:</b> 78.40% (24.91% std. deviation)"
var feb122019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "Early dismissal chance", "Snow day chance"],
    [new Date("2/9/2019 06:30:00 GMT-0500"), 0, 0, 5, 42],
    [new Date("2/10/2019 12:00:00 GMT-0500"), null, null, null, 65],
    [new Date("2/11/2019 07:00:00 GMT-0500"), null, null, null, 85],
    [new Date("2/11/2019 13:30:00 GMT-0500"), null, null, null, 100],
    [new Date("2/11/2019 19:30:00 GMT-0500"), null, null, null, 100]
]

var feb132019_summary = ["February 13, 2019", "2-hour delay", "Success", "4"]
var feb132019_avg = "<b>Average 1-hour delay chance:</b> 56.50% (42.51% std. deviation)<br><b>Average 2-hour delay chance:</b> 73.75% (26.89% std. deviation)<br><b>Average 3-hour delay chance:</b> 53.00% (16.06% std. deviation)<br><b>Average Snow Day chance:</b> 15.25% (10.24% std. deviation)"
var feb132019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("2/12/2019 16:30:00 GMT-0500"), 50, 40, 29, 20],
    [new Date("2/12/2019 18:30:00 GMT-0500"), 78, 65, 59, 19],
    [new Date("2/12/2019 20:00:00 GMT-0500"), 98, 90, 62, 22],
    [new Date("2/13/2019 05:45:00 GMT-0500"), 0, 100, 62, 0]
]

var feb202019_summary = ["February 20, 2019", "Early dismissal", "Success", "7"]
var feb202019_avg = "<b>Average early dismissal chance:</b> 73.43% (19.14% std. deviation)<br><b>Average Snow Day chance:</b> 15.67% (6.03% std. deviation)"
var feb202019_data = [
    ["Prediction Time", "Early dismissal chance", "Snow day chance"],
    [new Date("2/18/2019 20:00:00 GMT-0500"), 45, null],
    [new Date("2/19/2019 08:45:00 GMT-0500"), 55, null],
    [new Date("2/19/2019 12:45:00 GMT-0500"), 70, 15],
    [new Date("2/19/2019 17:30:00 GMT-0500"), 75, 10],
    [new Date("2/19/2019 22:15:00 GMT-0500"), 78, 22],
    [new Date("2/20/2019 07:00:00 GMT-0500"), 91, null],
    [new Date("2/20/2019 09:00:00 GMT-0500"), 100, null]
]

var feb212019_summary = ["February 21, 2019", "2-hour delay", "Success", "4"]
var feb212019_avg = "<b>Average 1-hour delay chance:</b> 82.00% (18.57% std. deviation)<br><b>Average 2-hour delay chance:</b> 72.50% (18.43% std. deviation)<br><b>Average 3-hour delay chance:</b> 56.50% (10.60% std. deviation)<br><b>Average Snow Day chance:</b> 30.00% (10.65% std. deviation)"
var feb212019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("2/20/2019 10:30:00 GMT-0500"), 64, 55, 47, 23],
    [new Date("2/20/2019 18:00:00 GMT-0500"), 68, 59, 48, 19],
    [new Date("2/20/2019 20:00:00 GMT-0500"), 97, 83, 63, 37],
    [new Date("2/20/2019 22:15:00 GMT-0500"), 99, 93, 68, 41]
]

var feb252019_summary = ["February 25, 2019", "Nothing", "Failure", "2"]
var feb252019_avg = "<b>Average snow day chance:</b> 47.50% (0.04% std. deviation)"
var feb252019_data = [
    ["Prediction Time", "Snow day chance"],
    [new Date("2/24/2019 17:00:00 GMT-0500"), 50],
    [new Date("2/24/2019 17:01:00 GMT-0500"), 45]
]

var feb282019_summary = ["February 28, 2019", "Nothing", "Success", "3"]
var feb282019_avg = "<b>Average 1-hour delay chance:</b> 29.00% (8.54% std. deviation)<br><b>Average 2-hour delay chance:</b> 25.67% (9.02% std. deviation)<br><b>Average 3-hour delay chance:</b> 1.00% (1.00% std. deviation)<br><b>Average Snow Day chance:</b> 0.33% (0.58% std. deviation)"
var feb282019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"], 
    [new Date("2/26/2019 17:00:00 GMT-0500"), 28, 25, 0, 0],
    [new Date("2/27/2019 11:00:00 GMT-0500"), 38, 35, 2, 1],
    [new Date("2/27/2019 15:15:00 GMT-0500"), 21, 17, 1, 0]
]

var mar42019_summary = ["March 4, 2019", "2-hour delay chance", "Failure", "6"]
var mar42019_avg = "<b>Average 1-hour delay chance:</b> 32.40% (25.11% std. deviation)<br><b>Average 2-hour delay chance:</b> 98.20% (1.92% std. deviation)<br><b>Average 3-hour delay chance:</b> 88.83% (7.33% std. deviation)<br><b>Average Snow Day chance:</b> 71.67% (9.42% std. deviation)"
var mar42019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("3/1/2019 09:30:00 GMT-0500"), 65, 95, 85, 58],
    [new Date("3/2/2019 16:00:00 GMT-0500"), 48, 98, 78, 63],
    [new Date("3/3/2019 14:00:00 GMT-0500"), 29, 99, 85, 71],
    [new Date("3/3/2019 16:00:00 GMT-0500"), 20, 99, 97, 79],
    [new Date("3/3/2019 18:50:00 GMT-0500"), 0, 100, 93, 81],
    [new Date("3/3/2019 21:30:00 GMT-0500"), null, null, 95, 78]
]

// 2019-2020 chart data

var nov122019_summary = ["November 12, 2019", "Nothing", "Success", "6"]
var nov122019_avg = "<b>Average 1-hour delay chance:</b> 0.00% (0.00% std. deviation)<br><b>Average 2-hour delay chance:</b> 8.80% (9.37% std. deviation)<br><b>Average 3-hour delay chance:</b> 13.40% (12.24% std. deviation)<br><b>Average Snow Day chance:</b> 10.67% (9.54% std. deviation)"
var nov122019_data = [
    ["Prediction Time", "1-hour delay chance", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("11/9/2019 13:30:00 GMT-0500"), 0, 15, 20, 15],
    [new Date("11/9/2019 22:00:00 GMT-0500"), 0, 5, 10, 14],
    [new Date("11/10/2019 13:30:00 GMT-0500"), null, null, null, 5],
    [new Date("11/10/2019 20:30:00 GMT-0500"), null, 22, 31, 26],
    [new Date("11/11/2019 11:30:00 GMT-0500"), null, 2, 6, 4],
    [new Date("11/12/2019 06:00:00 GMT-0500"), 0, 0, 0, 0]
]

var nov182019_summary = ["November 18, 2019", "Nothing", "Failure", "4"]
var nov182019_avg = "<b>Average 2-hour delay chance:</b> 33.75% (22.87% std. deviation)<br><b>Average 3-hour delay chance:</b> 17.50% (14.43% std. deviation)<br><b>Average Snow Day chance:</b> 17.50% (25.33% std. deviation)"
var nov182019_data = [
    ["Prediction Time", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("11/17/2019 10:00:00 GMT-0500"), 40, 35, 55],
    [new Date("11/17/2019 15:30:00 GMT-0500"), 45, 20, 10],
    [new Date("11/17/2019 19:00:00 GMT-0500"), 50, 15, 5],
    [new Date("11/18/2019 06:00:00 GMT-0500"), 0, 0, 0]
]

var nov192019_summary = ["November 19, 2019", "Nothing", "Failure", "4"]
var nov192019_avg = "<b>Average 2-hour delay chance:</b> 50.00% (35.36% std. deviation)<br><b>Average 3-hour delay chance:</b> 66.67% (27.77% std. deviation)<br><b>Average Snow Day chance:</b> 25.50% (17.92% std. deviation)"
var nov192019_data = [
    ["Prediction Time", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("11/18/2019 11:00:00 GMT-0500"), 50, 45, 30],
    [new Date("11/18/2019 15:50:00 GMT-0500"), 74, 57, 42],
    [new Date("11/18/2019 21:30:00 GMT-0500"), 76, 60, 30],
    [new Date("11/19/2019 06:00:00 GMT-0500"), 0, 0, 0]
]

var dec22019_summary = ["December 2, 2019", "Snow Day", "Success", "8"]
var dec22019_avg = "<b>Average Snow Day chance:</b> 83.88% (23.67% std. deviation)"
var dec22019_data = [
    ["Prediction Time", "Snow day chance"],
    [new Date("11/28/2019 20:40:00 GMT-0500"), 35],
    [new Date("11/29/2019 11:15:00 GMT-0500"), 60],
    [new Date("11/29/2019 17:45:00 GMT-0500"), 90],
    [new Date("11/30/2019 10:00:00 GMT-0500"), 93],
    [new Date("11/30/2019 13:30:00 GMT-0500"), 95],
    [new Date("12/1/2019 11:00:00 GMT-0500"), 98],
    [new Date("12/1/2019 17:40:00 GMT-0500"), 100],
    [new Date("12/1/2019 18:55:00 GMT-0500"), 100]
]

var dec32019_summary = ["December 3, 2019", "2-hour delay", "Success", "6"]
var dec32019_avg = "<b>Average 2-hour delay chance:</b> 95.33% (5.35% std. deviation)<br><b>Average 3-hour delay chance:</b> 60.67% (29.85% std. deviation)<br><b>Average Snow Day chance:</b> 35.00% (18.09% std. deviation)"
var dec32019_data = [
    ["Prediction Time", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("12/1/2019 19:15:00 GMT-0500"), 85, 70, 40],
    [new Date("12/2/2019 10:35:00 GMT-0500"), 98, 77, 44],
    [new Date("12/2/2019 16:15:00 GMT-0500"), 95, 70, 45],
    [new Date("12/2/2019 19:15:00 GMT-0500"), 96, 72, 32],
    [new Date("12/2/2019 23:30:00 GMT-0500"), 98, 75, 49],
    [new Date("12/3/2019 05:30:00 GMT-0500"), 100, 0, 0]
]

var dec112019_summary = ["December 11, 2019", "2-hour delay", "Failure", "3"]
var dec112019_avg = "<b>Average 2-hour delay chance:</b> 17.00% (2.65% std. deviation)<br><b>Average Snow Day chance:</b> 2.50% (2.12% std. deviation)"
var dec112019_data = [
    ["Prediction Time", "2-hour delay chance", "Snow day chance"],
    [new Date("12/10/2019 10:30:00 GMT-0500"), 20, null],
    [new Date("12/10/2019 18:15:00 GMT-0500"), 16, 4],
    [new Date("12/10/2019 22:00:00 GMT-0500"), 15, 1]
]

var dec172019_summary = ["December 17, 2019", "Snow Day", "Success", "4"]
var dec172019_avg = "<b>Average Snow Day chance:</b> 73.75% (28.69% std. deviation)"
var dec172019_data = [
    ["Prediction Time", "Snow day chance"],
    [new Date("12/15/2019 13:00:00 GMT-0500"), 40],
    [new Date("12/15/2019 18:00:00 GMT-0500"), 60],
    [new Date("12/16/2019 07:00:00 GMT-0500"), 95],
    [new Date("12/16/2019 14:15:00 GMT-0500"), 100]
]

var dec182019_summary = ["December 18, 2019", "Nothing", "Somewhat successful", "5"]
var dec182019_avg = "<b>Average 2-hour delay chance:</b> 61.40% (8.79% std. deviation)<br><b>Average 3-hour delay chance:</b> 21.20% (13.22% std. deviation)<br><b>Average Snow Day chance:</b> 13.60% (7.83% std. deviation)"
var dec182019_data = [
    ["Prediction Time", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("12/17/2019 06:15:00 GMT-0500"), 70, 35, 15],
    [new Date("12/17/2019 10:00:00 GMT-0500"), 72, 36, 16],
    [new Date("12/17/2019 15:30:00 GMT-0500"), 55, 15, 0],
    [new Date("12/17/2019 20:00:00 GMT-0500"), 55, 10, 20],
    [new Date("12/17/2019 22:00:00 GMT-0500"), 55, 10, 17]
]

var jan62020_summary = ["January 6, 2020", "Nothing", "Somewhat successful", "2"]
var jan62020_avg = "<b>Average 2-hour delay chance:</b> 45.00% (0.00% std. deviation)"
var jan62020_data = [
    ["Prediction Time", "2-hour delay chance"],
    [new Date("1/5/2020 17:30:00 GMT-0500"), 45],
    [new Date("1/5/2020 22:00:00 GMT-0500"), 45]
]

var feb62020_summary = ["February 6, 2020", "Nothing", "Failure", "7"]
var feb62020_avg = "<b>Average 2-hour delay chance:</b> 92.50% (2.89% std. deviation)<br><b>Average 3-hour delay chance:</b> 83.67% (4.76% std. deviation)<br><b>Average Snow Day chance:</b> 60.29% (18.59% std. deviation)"
var feb62020_data = [
    ["Prediction Time", "2-hour delay chance", "3-hour delay chance", "Snow day chance"],
    [new Date("2/3/2020 09:00:00 GMT-0500"), null, null, 55],
    [new Date("2/4/2020 07:15:00 GMT-0500"), null, 85, 75],
    [new Date("2/4/2020 16:00:00 GMT-0500"), null, 92, 85],
    [new Date("2/5/2020 10:30:00 GMT-0500"), 90, 85, 75],
    [new Date("2/5/2020 14:30:00 GMT-0500"), 90, 80, 55],
    [new Date("2/5/2020 22:30:00 GMT-0500"), 95, 80, 35],
    [new Date("2/6/2020 00:45:00 GMT-0500"), 95, 80, 42]
]