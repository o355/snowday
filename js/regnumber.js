// This code is licensed under the MIT License. Please see LICENSE.txt in the base directory for more information.

var countdown = 10;
var countdowninterval;
var toolongtimeout;
function addNumber() {
    var number = document.getElementById("number").value
    // Replace the URL here with the URL of your Snow Day API instance, making sure that it has the ending path set to /api/v2/registerNumber (or otherwise if you modified your)
    // instance of the Snow Day API
    var url = "https://example.com/api/v2/registerNumber"
    var method = "POST";
    var async = true;
    var formdata = new FormData();

    // Put your key in here for the registerNumber instance.
    var formdata_temp = {"number": number.replace("(", "").replace(")", ""), "key": "1234abcd"}

    for(name in formdata_temp) {
        formdata.append(name, formdata_temp[name]);
    }

    var request = new XMLHttpRequest();

    request.onload = function () {
        var data = request.responseText;
        var obj = JSON.parse(data)
        console.log(data)
        var code = obj['code'].toString()
        $("#toolong-text").attr("hidden", true)
        clearTimeout(toolongtimeout)
        $("#closingtext").attr("hidden", false)
        countdowninterval = setInterval(countdowntext, 1000)
        if (code == "59") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          // If you have a status page, then you can set the URL here.
          document.getElementById("progress").innerHTML = "Failed to add your number, the API is down for maintenance. Check <a href='https://example.com' target='_blank'>https://example.com</a> for the latest status information."
        } else if (code == "57") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number. The API is closed for the season, check back on November 1, 2019!"
        } else if (code == "55") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number. The API is closed at this time."
        } else if (code == "60") {
          toggleFailure()
          var banreason = obj['data']['reason'].toString()
          setTimeout(toggleFailure, 10050)
          $("#progress").html("Your number is banned from the Snow Day SMS Service. The reason for the ban is: " + banreason + ". Contact support for information about appealing this ban.")
        } else if (code == "62") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number due to your number not having a valid area code. Numbers based in the mainland US, Hawaii, and Alaska can be registered. If you believe this is in error, contact support."
        } else if (code == "58") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number due to invalid characters. The API filters out (, ), -, and +1, but can't filter out other characters. Check the input for any extra characters, and try again."
        } else if (code == "39") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "The number you entered had a length of 0 characters. Please input a number to register!"
        } else if (code == "20") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "The number you entered is not compatible with the Snow Day SMS Service due to carrier fees. This will occur if your number is registered with US Cellular. Click <a href='https://owenthe.dev/snowday/support/kb/faq.php?id=25' target='_blank'>here</a> for more info."
        } else if (code == "33") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "The Snow Day SMS Service has reached the 126 user limit. Please try again when some users have unregistered. If you want this limit increased, we encourage you to donate!"
        } else if (code == "54") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Whoops! An internal error occurred on our end. Please try again later, and contact support if this keeps happening."
        } else if (code == "22") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "When waiting for the verification code status, a timeout error occurred. Make sure your phone is on and can receive SMS messages, then try again."
        } else if (code == "13") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number, as your number wasn't reachable. Make sure your phone is on, and can receive SMS messages, then try again."
        } else if (code == "14") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number, the confirmation message was blocked, and was unable to send. Make sure that (845) 245-6252 and (845) 245-6118 is unblocked on your phone, and with your carrier."
        } else if (code == "15") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number, an unknown destination error occurred. Make sure your phone is on, and that you entered a mobile number capable of receiving SMS messages."
        } else if (code == "16") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number, you entered a landline phone number. Make sure you enter a mobile phone number that can receive SMS messages."
        } else if (code == "17") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number, your carrier flagged our confirmation messages as spam. Wait a few days, then try to sign up again."
        } else if (code == "18") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Failed to add your number, an unknown error occurred. Make sure your phone is on, and can receive SMS texts."
        } else if (code == "19") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Whoops! An unknown error occurred. Try again later."
        } else if (code == "8") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Your number is currently pending verification. If you want to subscribe to the SMS Service, text back Y to (845) 245-6252. Otherwise, wait ~72 hours, or contact support."
        } else if (code == "5") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Whoops! The number you entered includes extra characters. Make sure there aren't any letters or symbols other than (, ), or - in the input field."
        } else if (code == "4") {
          toggleSuccess()
          setTimeout(toggleSuccess, 10050)
          document.getElementById("progress").innerHTML = "Your number is already subscribed, no further action is needed. If you want to unsubscribe, text back UNSUB."
        } else if (code == "3") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          document.getElementById("progress").innerHTML = "Whoops! The number you entered didn't have a valid length. Make sure you enter a number with the area code & local number."
        } else if (code == "6") {
          toggleFailure()
          setTimeout(toggleFailure, 10050)
          // Put in the Twilio SMS numbers that you're using for the Snow Day SMS Service here.
          document.getElementById("progress").innerHTML = "Whoops! We couldn't send the confirmation text to your number due to a full message block, or a Twilio error. Text START to (123) 456-7890, and try again."
        } else if (code == "0") {
          toggleSuccess()
          setTimeout(toggleSuccess, 10050)
          // Put in the Twilio SMS numbers that you're using for the Snow Day SMS Service here.
          document.getElementById("progress").innerHTML = "Hurray! Your number is now pending verification. Check for a text from (123) 456-7890 to confirm that you want to subscribe. If you don't respond within 72 hours, you'll need to request registration again."}
  
        setTimeout(function() {$('#modal2').modal('close')}, 10000)
        setTimeout(function() {document.getElementById("progress").innerHTML = "Signing you up for the Snow Day SMS Service, hold tight!"}, 10100)
    }

    request.onerror = function () {
        $("#toolong-text").attr("hidden", true)
        toggleFailure()
        setTimeout(toggleFailure, 10050)
        document.getElementById("progress").innerHTML = "An error occurred while registering your number. Please try again later, and contact support if this keeps happening."
        setTimeout(function() {$('#modal2').modal('close')}, 10000)
        setTimeout(function() {document.getElementById("progress").innerHTML = "Signing you up for the Snow Day SMS Service, hold tight!"}, 10100)
    }

    request.ontimeout = function () {
        $("#toolong-text").attr("hidden", true)
        toggleFailure()
        setTimeout(toggleFailure, 10050)
        document.getElementById("progress").innerHTML = "A timeout error occurred while registering your number. Check that you have a working internet connection, then try again."
        setTimeout(function() {$('#modal2').modal('close')}, 10000)
        setTimeout(function() {document.getElementById("progress").innerHTML = "Signing you up for the Snow Day SMS Service, hold tight!"}, 10100)
    }

    request.timeout = 90000
    request.open(method, url, async)
    request.send(formdata)
    toolongtimeout = setTimeout(function () {$("#toolong-text").attr("hidden", false)}, 15000)

}

function toggleSuccess() {
    $(".circle-loader").removeClass("load-error").toggleClass("load-complete");
    $(".checkmark").removeClass("error").addClass("draw").toggle();
}

function toggleFailure() {
    $(".circle-loader").removeClass("load-complete").toggleClass("load-error");
    $(".checkmark").removeClass("draw").addClass("error").toggle();
}

function countdowntext() {
    countdown = countdown - 1
    if (countdown == 0) {
      $("closingtext-span").html("Closing in 0 seconds...")
      setTimeout(function () {
        $("#closingtext").attr("hidden", true)
        $("#closingtext-span").html("Closing in 10 seconds...")
        clearInterval(countdowninterval)
        countdown = 10
      }, 150)
    } else {
        $("#closingtext-span").html("Closing in " + countdown + " seconds...")
    }
}