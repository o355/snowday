// This code is licensed under the MIT License. Please see LICENSE.txt in the base directory for more information.

function drawTrend(summary, data, avgsdata, static) {
    var instance = M.Modal.getInstance(chartmodal); instance.open()
    if (data.length == 2) {
        $("#mainchart").html("<h5>There is only 1 prediction available for this day. As a result, the dynamic chart is unavailable. <br>Please use the static image instead.</h5>")
        $("#chart-href").attr("href", "/charts/" + static + ".png")
        $("#details_date").html(summary[0]);
        $("#details_outcome").html(summary[1]);
        $("#details_predictionoutcome").html(summary[2]);
        $("#details_numofpredictions").html(summary[3]);
        $("#details_avgs").html(avgsdata);
        
        return
    }

    $("#details_date").html(summary[0]);
    $("#details_outcome").html(summary[1]);
    $("#details_predictionoutcome").html(summary[2]);
    $("#details_numofpredictions").html(summary[3]);
    $("#details_avgs").html(avgsdata);

    var data = new google.visualization.arrayToDataTable(data);

    var options = {
        title: summary[0] + ' Prediction Trends',
        curveType: 'function',
        legend: {position: 'top'},
        vAxis: {viewWindow: {max: 100, min: 0}, format: "#" + "'%'"},
        hAxis: {format: 'M/d/y h:mm a'},
        backgroundColor: "#fafafa",
        pointSize: 2,
        chartArea: {width: "75%", height: "80%"}
    }

    var dateFormatter = new google.visualization.DateFormat({pattern: 'M/d/y h:mm a'})
    dateFormatter.format(data, 0)
    var formatter = new google.visualization.NumberFormat({
        fractionDigits: 0,
        suffix: '%'
    });
    formatter.format(data, 1)
    try {
        formatter.format(data, 2)
        formatter.format(data, 3)
        formatter.format(data, 4)
        formatter.format(data, 5)
        formatter.format(data, 6)
        formatter.format(data, 7)
    } catch (e) {

    }
    var chart = new google.visualization.LineChart(document.getElementById("mainchart"));
    $("#chart-href").attr("href", "/charts/" + static + ".png")
    setTimeout(function () {chart.draw(data, options)}, 30)

}