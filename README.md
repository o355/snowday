# Snow Day Predictor
HTML/JS/Materialize-based Snow Day Predictor used at owenthe.dev/snowday.

# Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

Snow Day Predictor Spaghet-o-meter: **2/10**

The Snow Day Predictor is generally not spaghetti code given the architecture. There's definitely some reusable code throughout the predictor, seen best with chart drawing & registering numbers. Chart data was hardcoded into a JS file, but this could have been achieved with a JSON file. The calculation of standard deviation & averages could have also been achieved with standard code and not hardcoded HTML. There's no long inline scripts, and code is pretty readable.

There are hardcoded variables in the regnumber.js file, which should have been moved to the top of the file for easier configuration (or moved to a separate config file if using something like React)

Other than that, it isn't truly awful. Flask could have definitely helped when it came to cleaning up repetitive code with trend data and replacing certain sections with separate HTML files. But, it should be noted that this was designed with vanilla HTML/JS with no server-side framework - so points aren't docked there.

# Getting started with the predictor page
You will need:
* A computer
* Some sort of web server to host the predictor on
* (Optional) A Snow Day API setup for the SMS signup portion of the website to work

You should be able to download the contents of the repository, throw it in a web server, and that's it. From there, you'll obviously need to make the necessary modifications to the webpage if you intend on making your own website based off of this repo.

If you do set up the Snow Day API for SMS signups, make sure you fully follow the documentation on the Snow Day API repo, and change the URL/API key in js/regnumber.js so that your Snow Day SMS signups will work.

# Other repositories
There is the Snow Day Dashboard repository, which is the dashboard used for customizing the Snow Day SMS Service experience.

The Snow Day API powers the entirety of the Snow Day SMS Service & Dashboard.

There is another repo called Snow Day Tools, which has a collection of tools available for management of the Snow Day API.

# License
The Snow Day Predictor is licensed under the MIT License. It switched to the MIT License on May 19, 2020. Before that, the predictor was licensed under the MPL Version 2.0.
